# Pipeline with custom payload that creates issue

This project is intended to demonstrate the use of a pipeline to process a custom payload with custom logic, create issues (and Service Desk issues in the future) and optional comments with additional information to the sender of the inquiry (like company, size, SLA etc.).

You can find some example issues that were created by the pipeline in the [issues section](https://gitlab.com/gitlab-org/incubation-engineering/service-desk/pipeline-with-custom-payload-that-creates-issue/-/issues).

## Trigger pipeline via API

You need a Project Access Token, so the pipeline can add an issue via the API and a Pipeline Trigger Token (via Settings -> CI/CD) to trigger a pipeline (with custom payload).

```shell
curl -X POST \
     --fail \
     -F token=[REDACTED] \
     -F "ref=main" \
     -F "variables[WEBFORM_NAME]=Jane Doe" \
     -F "variables[WEBFORM_EMAIL]=jdoe@example.com" \
     https://gitlab.com/api/v4/projects/[PROJECT_ID]/trigger/pipeline
```

## Further Reading
- https://docs.gitlab.com/ee/ci/triggers/#use-a-cicd-job
- https://docs.gitlab.com/ee/api/issues.html#new-issue
