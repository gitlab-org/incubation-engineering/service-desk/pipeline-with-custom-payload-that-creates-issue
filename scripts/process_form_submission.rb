# frozen_string_literal: true

require "uri"
require "net/http"
require "json"

payload_file_path = ENV["TRIGGER_PAYLOAD"]
project_id = ENV["CI_PROJECT_ID"]
access_token = ENV["PROJECT_ACCESS_TOKEN"]
return if payload_file_path.nil?

payload_text = File.read(payload_file_path)
payload = JSON.parse payload_text
variables = payload["variables"]

name = variables["WEBFORM_NAME"]
email = variables["WEBFORM_EMAIL"]

# Add some custom logic to validate the sender is a user of your product.

query_params = {
  title: "Issue from #{name}",
  description: "- Email: #{email}\n/estimate 1h\n/weight 3\n/label ~triage\n/assign @msaleiko"
}

issue_uri = URI("https://gitlab.com/api/v4/projects/#{project_id}/issues?#{URI.encode_www_form(query_params)}")

req = Net::HTTP::Post.new(issue_uri)
req["PRIVATE-TOKEN"] = access_token
res = Net::HTTP.start(issue_uri.hostname, issue_uri.port, use_ssl: true) do |http|
  http.request(req)
end

issue_iid = JSON.parse(res.body)["iid"]

# After we added the issue, we could add additional information to the issue using our crm tools.
# In this example we just add a new note that lists some customer details and adds a sla label

comment_body = "#{name} is a user of our customer **Important Inc**\n- Size: mid\n- Seats: 30\n- SLA: 1d\n\n/label ~sla::1d\nApplied ~sla::1d label."

query_params = {
  body: comment_body,
  internal: true
}

note_uri = URI("https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_iid}/notes?#{URI.encode_www_form(query_params)}")
req = Net::HTTP::Post.new(note_uri)
req["PRIVATE-TOKEN"] = access_token
res = Net::HTTP.start(note_uri.hostname, note_uri.port, use_ssl: true) do |http|
  http.request(req)
end